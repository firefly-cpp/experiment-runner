defmodule ExperimentRunner.MixProject do
  use Mix.Project

  def project do
    [
      app: :experiment_runner,
      version: "0.1.2",
      description:
        "A software package for easier sharing, maintaining, and running scientific experiments.",
      elixir: "~> 1.14",
      start_permanent: Mix.env() == :prod,
      package: package(),
      deps: deps(),

      # Docs
      name: "ExperimentRunner",
      source_url: "https://gitlab.com/firefly-cpp/experiment-runner",
      homepage_url: "https://gitlab.com/firefly-cpp/experiment-runner",
      docs: [
        main: "ExperimentRunner",
        # logo: "path/to/logo.png",
        extras: ["README.md"]
      ]
    ]
  end

  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp package() do
    [
      name: "experiment_runner",
      files: ~w(lib mix.exs README.md LICENSE),
      licenses: ["MIT"],
      links: %{"GitLab" => "https://gitlab.com/firefly-cpp/experiment-runner"}
    ]
  end

  defp deps do
    [
      {:httpoison, "~> 2.0"},
      {:yaml_elixir, "~> 2.9.0"},
      {:git_cli, "~> 0.3"},
      {:ex_doc, "~> 0.21", only: :dev, runtime: false}
    ]
  end
end
