defmodule ExperimentRunner do
  @moduledoc """
  ExperimentRunner is a software package for easier sharing, maintaining, and running scientific experiments. The entire package is designed to be easily integrated into a container environment and allow easier reproducibility of scientific experiments.
  """

  @doc """
  Main entrypoint for processing the experiment.

  Returns `:ok`.

  ## Examples

      iex> ExperimentRunner.start_experiment
      :ok
  """

  def start_experiment(yaml_file) do
    IO.puts("#{IO.ANSI.blue()}Reading experiment configuration.")
    content = ReadConfig.read_yaml(yaml_file)
    IO.puts("#{IO.ANSI.blue()}Done.")
    IO.puts("#{IO.ANSI.blue()}Preprocessing experiment file.")
    ReadConfig.preprocess_yaml(elem(content, 1))
    IO.puts("#{IO.ANSI.blue()}Done.")
  end
end
