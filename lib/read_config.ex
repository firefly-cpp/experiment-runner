defmodule ReadConfig do
  @moduledoc """
  Documentation for `ReadConfig`.
  """

  @doc """
  Parse Yaml file.

  ## Parameters

    - file: Filename where configuration is stored.

  ## Examples

      iex> ReadConfig.read_yaml("nia-experiment.yml")
      "TODO"
  """

  def read_yaml(file) do
    path = Path.join(File.cwd!(), file)
    YamlElixir.read_from_file(path, atoms: true)
  end

  @doc """
  Download dataset from the specified URL

  ## Parameters

    - dataset: Dataset URL.
    - hash: Hash of a file.
    - path: Folder where downloaded dataset is stored.
  ## Examples

      iex> ReadConfig.download_dataset("https://raw.githubusercontent.com/lukapecnik/NiaClass/master/examples/example_files/dataset.csv", "hash", "datasets/")
      "TODO"
  """
  def download_dataset(dataset, hash, path) do
    IO.puts("Downloading: #{dataset}")
    %HTTPoison.Response{body: body} = HTTPoison.get!(dataset)
    # get the filename of a dataset
    filename = dataset |> String.split("/") |> List.last()
    File.write!(Path.join(path, filename), body)
    hash1 = :crypto.hash(:sha256, body) |> Base.encode16() |> String.downcase()

    if hash1 == hash do
      IO.puts("Hash is correct!")
    else
      IO.puts("Hashes are not matching!")
      IO.puts("#{IO.ANSI.cyan()}got: #{hash1}")
    end
  end

  @doc """
  User-friendly preprocessing of Yaml file.

  ## Parameters

    -  content: Parsed Yaml file content.

  ## Examples

      iex> ReadConfig.preprocess_yaml("yaml_content"")
      "TODO"
  """
  def preprocess_yaml(content) do
    IO.puts("#{IO.ANSI.cyan()}Starting experimental environment.")
    IO.puts("----------------------------------------------------")
    IO.puts("Description of experiment: #{content["description"]}")
    IO.puts("Version: #{content["version"]}")
    IO.puts("Last revision: #{content["last_revision"]}")
    IO.puts("----------------------------------------------------")
    IO.puts("#{IO.ANSI.yellow()}Downloading datasets.")

    # create fresh folder for datasets
    File.mkdir("experimental-env")

    # download all datasets
    for val <- content["datasets"],
        do: download_dataset(val["url"], val["hash"], "experimental-env/")

    File.cd("experimental-env/")
    # clone all repositories
    for val <- content["repositories"], do: clone_repo(val)

    # run all scripts
    for script <- content["scripts"],
        do: run_experiment_script(script["name"], script["lang"], script["command"])
  end

  @doc """
  Main entrypoint for running experiment scripts.

  ## Parameters

    - language: atom (programming language)
    - script: name of script to be run

  ## Examples

      iex> ReadConfig.run_experiment_script("script1", :python, "run.py")
      "TODO"
  """

  def run_experiment_script(name, language, script) do
    IO.puts("#{IO.ANSI.blue()}Running experiment script: #{name}")
    RunScript.run(language, script)
    IO.puts("#{IO.ANSI.blue()}Done.")
  end

  @doc """
  Simple procedure for cloning Git repository.

  ## Parameters

    - repo: Git repository

  ## Examples

      iex> ReadConfig.clone_repo("https://gitlab.com/firefly-cpp/nia-test-experiments")
      "TODO"
  """

  def clone_repo(repo) do
    IO.puts("#{IO.ANSI.green()}Cloning repository: #{repo}")
    Git.clone(repo)
    IO.puts("#{IO.ANSI.green()}Cloning DONE!")
  end
end
