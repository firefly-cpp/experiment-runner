defmodule RunScript do
  @moduledoc """
  Documentation for `RunScript`.
  """

  @doc """
  Run scripts.

  ## Parameters

    - language: atom (:python, :bash, :ruby)
    - script: filename (run.py, run.rb)
  ## Examples

      iex> RunScript.run(:python, "run.py")
      "TODO"
  """

  def run(:python, script) do
    System.cmd("python", [script])
  end

  def run(:bash, script) do
    System.cmd("sh", [script])
  end

  def run(:ruby, script) do
    System.cmd("ruby", [script])
  end
end
