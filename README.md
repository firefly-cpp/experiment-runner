# ExperimentRunner

ExperimentRunner is a software package for easier sharing, maintaining, and running scientific experiments. The entire package is designed to be easily integrated into a container environment and allow easier reproducibility of scientific experiments.

Current features of this package:
- The experiment is defined in a Yaml file so it can be easily shared among platforms.
- Datasets can be automatically downloaded from online repositories.
- All datasets are checked against the hash to ensure that datasets match and are not altered during any phases.
- Source code can be cloned from remote repositories.
...

## Installation

The package can be installed by adding `experiment_runner` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:experiment_runner, "~> 0.1.0"}
  ]
end
```

## Examples

### How to define an experiment in the YAML file?

```yaml
name: Nia experiment
description: NiaPy test suite
version: 0.1
last_revision: 16.2.2023
datasets:
  - url: >-
      https://archive.ics.uci.edu/ml/machine-learning-databases/abalone/abalone.data
    hash: de37cdcdcaaa50c309d514f248f7c2302a5f1f88c168905eba23fe2fbc78449f
  - url: 'https://archive.ics.uci.edu/ml/machine-learning-databases/wine/wine.data'
    hash: 6be6b1203f3d51df0b553a70e57b8a723cd405683958204f96d23d7cd6aea659
  - url: >-
      https://raw.githubusercontent.com/lukapecnik/NiaClass/master/examples/example_files/dataset.csv
    hash: 506a7faf88b6a57a7156aba3993ee131cb6fe34d8d8137c1d7904c380b73c426
repositories:
  - 'https://gitlab.com/firefly-cpp/nia-test-experiments'
scripts:
  - name: script1
    lang: ':python'
    command: nia-test-experiments/src/niapy_custom_problem.py
  - name: script2
    lang: ':python'
    command: nia-test-experiments/src/niapy_pso.py
  - name: script3
    lang: ':bash'
    command: nia-test-experiments/run.sh

```

## License
This package is distributed under the MIT License. This license can be found online at <http://www.opensource.org/licenses/MIT>.

## Disclaimer
This framework is provided as-is, and there are no guarantees that it fits your purposes or that it is bug-free. Use it at your own risk!
